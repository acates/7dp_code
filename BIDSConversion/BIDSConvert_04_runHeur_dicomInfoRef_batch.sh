#!/bin/bash
#dont forget to set execute permissions with chmod a+x name.sh before executing
#execute with ./name.sh even if you're already in the folder, or bash name.sh

# Runs the heudiconv conversion using the BIDSConvert_03_heur_dicominfoRef.py
# First compares the dicominfo of each participant to the refernce to see if the heuristic
# is appropriate.

# h2mri-update: enter your participant ids below
    # 201 103 104 204 105 205 106 209 206 211 107 108 203 213 212 109 214 110 207 111 216 217 218 219 115 221 220 119 224 222 116 228 125 127 232; do
for subject in 201 104 204 105 205 106 209 206 211 107 108 203 213 212 109 214 110 207 111 216 217 218 219 115 221 220 119 224 222 116 228 125 127 232; do
    echo "-- pxID: " $subject
    echo "-- Comparing dicominfo.tsv to reference"
    # compare dicominfo of this participant to our saved reference
    # https://stackoverflow.com/questions/11900828/store-return-value-of-a-python-script-in-a-bash-script
    # $compDicom will be output as "true" or "fasle" depending on the result of the
    # BIDSConvert_04b_compareDicominfoToRef.py script.
    # BIDSConvert_04b_compareDicominfoToRef will take $subject as input, so each participant
    # will be compared as the loop iterates
    # BIDSConvert_04b_compareDicominfoToRef includes the name of the reference dicominfo.tsv file
    # If the dicominfo.tsv refernce filename is changed, it will need to be updated in the .py
    compDicom=$(python BIDSConvert_04a_compareDicominfoToRef.py $subject 2>&1)
    echo "-- Dicom match: " $compDicom

    # https://unix.stackexchange.com/questions/23961/how-do-i-exit-a-script-in-a-conditional-statement
    # if "true"
    # if $compDicom; then
    if [ "$compDicom" = "True" ]; then
        echo "-- Dicoms matches reference. Continuing with conversion using reference heuristic"
    else
        echo "-- WARNING: Dicoms do not match reference. This dataset may require a uniquely specified heuristic."
        echo "-- WARNING: Skipping this participant"
        # jumps to next itteration of script
        continue
    fi

    # h2mri-update: sepecify the correct path for your study folder
    # (unfortunately this bind and spcification of the singularity container cannot take a value of $dataRoot as input)
    # -b flag is for bids format. This should handle multiecho appropriately with the 0.5.4 heudiconv release (https://github.com/nipy/heudiconv/issues/162)
    singularity run -B \
    /vols/Scratch/brc_em/7DP:/base \
    /vols/Scratch/brc_em/7DP/code_AdeC/BIDSConversion/singularityContainer_heudiconv_054.sif \
    -d /base/sourcedata/dicom/{subject}/* \
    -o /base/sourcedata/BIDS/ \
    -f /base/code_AdeC/BIDSConversion/BIDSConvert_03_heur_dicominfoRef.py \
    -s $subject  \
    -c dcm2niix -b \
    --overwrite

done
