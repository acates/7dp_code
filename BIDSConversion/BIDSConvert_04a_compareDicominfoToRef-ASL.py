#!/usr/bin/env fslpython

# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd
# from pandas.util.testing import assert_series_equal
# gives access to an argv list
import sys

# where all our dicom folders are
# dataRoot = '/Users/cassandragouldvanpraag/Documents/BIDS_forWINIT/2017_102'
# h2mri-update: dataRoot should point to your study folder
dataRoot = '/vols/Scratch/brc_em/7DP'

# reference dicom info tsv file
path_dicominfo_ref = os.path.join(dataRoot,'code_AdeC','BIDSConversion','BIDSConvert_02_dicominfoRef-ASL.tsv')
dicominfo_ref = pd.read_csv(path_dicominfo_ref,sep='\t')

# We want this to read in a specific pxID dicominfo.tsv from the .heudiconv folder.
# That pxID is taken as an input when we run this
# Cribbing from Paul's into to python jupyter notebook
# and https://www.pythonforbeginners.com/system/python-sys-argv

# the first argument passed (argv[0]) is the script name, so arvg[1] is the pxID to test
pxIDToTest = str(sys.argv[1])
path_dicominfo_pxToTest = os.path.join(dataRoot,'sourcedata','BIDS','.heudiconv',pxIDToTest,'info','dicominfo.tsv')
dicominfo_pxToTest = pd.read_csv(path_dicominfo_pxToTest,sep='\t')

# combining the series_id columns of the two dicominfo files so we can compare the strings
# https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html
# seems to handle columns of differnt lengths!
frames = [dicominfo_ref.series_id, dicominfo_pxToTest.series_id]
result = pd.concat(frames,axis=1)
# renaming the columns
result.columns = ['ref', 'test']

# compare the strings along the rows of ref and test
# http://www.datasciencemadesimple.com/string-compare-in-pandas-python-test-whether-two-strings-are-equal-2/
result['is_equal']= (result.ref==result.test)

# if all rows of is_equal are 1, this returnsd true. Else, it will be false.
dicomMatch = result['is_equal'].all()

# dicomMatch is returned to the main bash script to determine next action
# if dicomMatch, do BIDSConvert_03b
# if not kill and print some message
sys.exit(dicomMatch)
