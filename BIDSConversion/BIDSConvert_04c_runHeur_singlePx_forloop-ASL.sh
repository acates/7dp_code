#!/bin/bash
#dont forget to set execute permissions with chmod a+x name.sh before executing
#execute with ./name.sh even if you're already in the folder, or bash name.sh

# runs a specified heuristic file for the listed participants.
# Use this for cases where the refernce dicominfo doesn't match.

# h2mri-update: enter the name of your individually specified heuristic file
# pxHeuristicFile='BIDSConvert_04b_heur_102.py'

# h2mri-update: enter your participant ids below
# 112 113 117 118 122 123 124 125 127 201 210 223 225 227 230 232 999
for subject in 112 113 117 118 122 123 124 210 223 225 230 231; do
    echo "-- pxID: " $subject
    echo "-- Running heudiconv with non-typical heuristic"

    pxHeuristicFile='BIDSConvert_04b_heur_'$subject'-ASL.py'
    echo "participant heuristic file:" $pxHeuristicFile
    # Just leaving theis here for ref :)
    # # compare dicominfo
    # # https://stackoverflow.com/questions/11900828/store-return-value-of-a-python-script-in-a-bash-script
    # compDicom=$(python BIDSConvert_04b_compareDicominfoToRef.py $subject 2>&1)
    # echo "-- Dicom match: " $compDicom
    #
    # # https://unix.stackexchange.com/questions/23961/how-do-i-exit-a-script-in-a-conditional-statement
    # if $compDicom; then
    #     echo "-- Dicoms as expected. Continuing with conversion using reference heuristic"
    # else
    #     echo "-- WARNING: Dicoms do not match reference. This will have to be converted manually."
    #     echo "-- WARNING: Skipping this participant"
    #     # jumps to next itteration of script
    #     continue
    # fi
    #
    # #run heuristic file
    # docker run --rm -it \
    # -v /Users/cassandragouldvanpraag/Documents/BIDS_forWINIT/2017_102/dicom:/data:ro \
    # -v /Users/cassandragouldvanpraag/Documents/BIDS_forWINIT/2017_102/BIDS:/output \
    # nipy/heudiconv:latest \
    # -d /data/{subject}/* \
    # -s $subject \
    # -f /output/code/$pxHeuristicFile \
    # -c dcm2niix -b \
    # -o /output/

    # h2mri-update: sepecify the correct path for your study folder
    # (unfortunately this bind and spcification of the singularity container cannot take a value of $dataRoot as input)
    singularity run -B \
    /vols/Scratch/brc_em/7DP:/base \
    /vols/Scratch/brc_em/7DP/code_AdeC/BIDSConversion/singularityContainer_heudiconv_054.sif \
    -d /base/sourcedata/dicom/{subject}/* \
    -o /base/sourcedata/BIDS/ \
    -f /base/code_AdeC/BIDSConversion/$pxHeuristicFile \
    -s $subject  \
    -c dcm2niix -b

done
