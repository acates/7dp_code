#!/bin/bash

# Loops through the participant list to create the dicominfo.tsv file

# do your dicom directory names have spaces in? If so, keep in lines 3 & 4 below (https://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html), if not, comment out.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
# unpack individual sequence folders into one space for heudiconv (handles better than individual folders)
# h2mri-update: dataRoot should point to your study folder
dataRoot="/vols/Scratch/brc_em/7DP"

# h2mri-update: enter your participant ids in a list as below
# below is a list of all the PIDs which should match the reference participant
# 201 103 104 204 105 205 106 209 206 211 107 108 203 213 212 109 214 110 207 111 216 217 218 219 115 221 220 119 224 222 116 228 125 127 232; do
# below is a list of ALL PIDs
# 101 201 102 103 104 204 105 205 106 209 206 211 107 108 203 213 212 109 214 210 110 207 111 112 216 217 218 219 113 115 221 220 223 119 224 222 225 116 228 227 123 117 124 118 122 125 999 231 230 127 232
for subject in 101 201 102 103 104 204 105 205 106 209 206 211 107 108 203 213 212 109 214 210 110 207 111 112 216 217 218 219 113 115 221 220 223 119 224 222 225 116 228 227 123 117 124 118 122 125 999 231 230 127; do
    # expand the dicom folders as retrieved from calpendo into a single directory
    # ls -1d will list all the directories (1 level deep) under the particpant folder
    # each directory will then be $d
    # if [ -d "$dataRoot/sourcedata/dicom/$subject/*/" ]; then
    if [ "$(find $dataRoot/sourcedata/dicom/$subject/*/ -maxdepth 1 -type d -printf 1 | wc -m)" -gt 1 ]; then
        for d in $(ls -1d $dataRoot/sourcedata/dicom/$subject/*/); do
        echo "-- Unpacking $d"
        # move the contents of $d into the subejct directory
        mv $d/* $dataRoot/sourcedata/dicom/$subject/
        # remove $d
        rmdir $d
        done
    fi

    # This will use an image of heudiconv 0.5.4 pulled from dockerhub on 11/03/2019
    # via singularity pull --name singularityContainer_heudiconv_054.sif docker://nipy/heudiconv:0.5.4
    # It was included in the h2mri gitlab repository
    # tried pulling the "latest" version, but "debian" was the only tag available which wasn't "unstable" https://hub.docker.com/r/nipy/heudiconv/tags
    # h2mri-update: specify the correct path for your study folder
    # (unfortunately this bind and spcification of the singularity container cannot take a value of $dataRoot as input)

    singularity run -B \
    /vols/Scratch/brc_em/7DP:/base \
    /vols/Scratch/brc_em/7DP/code_AdeC/BIDSConversion/singularityContainer_heudiconv_054.sif \
    -d /base/sourcedata/dicom/{subject}/* \
    -o /base/sourcedata/BIDS/ \
    -f convertall \
    -s $subject  \
    -c none \
    --overwrite

    # deleting the pxID.edit.txt and pxID.auto.txt files so these are not automatically read when running the heuristic
    # see https://neurostars.org/t/overwriting-heudiconv-sub-auto-txt-and-sub-edit-txt/4172
    f_edit=$dataRoot/sourcedata/BIDS/.heudiconv/$subject/info/$subject.edit.txt
    f_auto=$dataRoot/sourcedata/BIDS/.heudiconv/$subject/info/$subject.auto.txt

    # only runs if we find the files, i.e. if heudiconv happend (avoids error message)
    if [ -f $f_edit ] && [ -f $f_auto ]; then
        echo "-- Deleting $f_edit"
        rm $f_edit
        echo "-- Deleting $f_auto"
        rm $f_auto
    fi

done
