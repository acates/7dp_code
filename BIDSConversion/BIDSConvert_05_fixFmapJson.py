# BIDSConvert_05_fixFmapJson.py
# see https://bids.neuroimaging.io/bids_spec1.0.2.pdf section 8.9
# for handelling the json files

# The fieldmaps were converted with some inconsistencies in how the echo times were numbered
# (this is a known bug)
# This script reads the filedmap .json files for all particpants converted and amends them
# if necessaryself.
# Also adds the "IntendedFor" information which is required for fmriprep to match tghe fieldmap
# to the BOLD sequence

import json
# library of functions for checking, makeing, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd
# for getting lists of files in directories
import glob

# h2mri-update: dataRoot should point to your BIDS folder
dataRoot = '/vols/Scratch/brc_em/test/sourcedata/BIDS'
# where BIDS folder is
# dataRoot = "/Users/cassandragouldvanpraag/Documents/BIDS_forWINIT/2017_102/BIDS/"

# tsv of participants created by heudiconv
pxID_data = pd.read_csv(os.path.join(dataRoot,'participants.tsv'),sep='\t')

for index, p in pxID_data.iterrows():
    path_pxFmap = os.path.join(dataRoot,p.participant_id,'fmap')
    print("\n-- Updating fmap json files in directory: ", path_pxFmap)

    # Add the IntendedFor information by reading in the acq-type from the file name

    # for all image types (mag1, mag2, phasediff)
    # https://stackoverflow.com/questions/13297406/using-file-extension-wildcards-in-os-listdirpath
    fList_alljson = glob.glob(os.path.join(path_pxFmap,'*.json'))

    for file in fList_alljson:
        print("-- Editing file: ", file)
        # pull the acqisition type from the file name
        # https://www.pythoncentral.io/cutting-and-slicing-strings-in-python/
        # h2mri: understand what the split function is achieving and correct the number of expected outputs to fit your data
        x,x,x,acqType,x = file.split("_")
        # print(acqType)

        # https://stackoverflow.com/questions/21035762/python-read-json-file-and-modify
        with open(file , "r+") as f:
            # print(file)
            data = json.load(f)
            # h2mri-update: enter the BOLD acquisitions ("str_intendedFor") which these fieldmaps should be used with based
            # on the labels which were created by your heuristic ("acq-type")(check BIDSConvert_03_heur_dicominfoRef.py for referece)
            # look to make sure str_intendedFor is appropriate if you have multiecho sequences
            if acqType == "acq-rest":
                str_intendedFor = "func/" + p.participant_id +"_task-rest_bold.nii.gz"
                print("-- Determined str IntendedFor: ", str_intendedFor)
            if acqType == "acq-task":
                str_intendedFor = [
                                    "func/" + p.participant_id +"_task-faces_bold.nii.gz",
                                    "func/" + p.participant_id +"_task-hipp_bold.nii.gz"
                                    ]
                print("-- Determined str IntendedFor: ", str_intendedFor)
            data["IntendedFor"] = str_intendedFor # <--- add `IntendedFor` value.

            f.seek(0)        # <--- should reset file position to the beginning.
            json.dump(data, f, indent=4)
            f.truncate()     # remove remaining part

    # for phasediff only
    # This is for handelling a specific error which came up in the validation that TE2-TE1 should be >0.
    # The heudiconv (dcm2niix) conversion seems to have named one of the acqistion echo times correctly, the other not.
    # Here we're going to check the values and swap them round if TE1>TE2.
    fList_phasediffjson = glob.glob(os.path.join(path_pxFmap,'*phasediff.json'))
    for file in fList_phasediffjson:
        print("-- Checking phasediff EchoTimes in file: ", file)
        # https://stackoverflow.com/questions/13949637/how-to-update-json-file-with-python
        with open(file, "r") as f:
            data = json.load(f)

            tmp_TE1 = data["EchoTime1"]
            tmp_TE2 = data["EchoTime2"]

            if tmp_TE1<tmp_TE2:
                print("-- EchoTime1 = ", tmp_TE1)
                print("-- EchoTime2 = ", tmp_TE2)
                continue
            else:
                data["EchoTime1"] = tmp_TE2
                data["EchoTime2"] = tmp_TE1

                print("-- Corrected EchoTime1 = ", tmp_TE2)
                print("-- Corrected EchoTime2 = ", tmp_TE1)

                with open(file, "w") as f:
                    json.dump(data, f, indent=4)
