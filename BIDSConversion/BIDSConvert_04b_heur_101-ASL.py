# BIDSConvert_03_heur_dicominfoRef.py

# Creates a "heuristic" for heudicomv to follow in converting the dicoms

import os

def create_key(template, outtype=('nii.gz',), annotation_classes=None):
    if template is None or not template:
        raise ValueError('Template must be a valid format string')
    return template, outtype, annotation_classes


def infotodict(seqinfo):
    """Heuristic evaluator for determining which runs belong where
    allowed template fields - follow python string module:
    item: index within category
    subject: participant id
    seqitem: run number during scanning
    subindex: sub index within group
    """

    # h2mri-update: create variables for each file type to be converted
    # then create the appropriate 'key' for conversion based on the BIDS format

    # SDLamo acquisitions generate 2 x mag (TE = 4.92 ms & TE = 7.38 ms) and 1 x
    # phasediff image => BIDS 8.9.1 Case 1
    # heudiconv / dcm2niix can see the different echo times (even though they are not apparent in the dicominfo.tsv)
    # and "not stacks" them i.e. seperately identifies as mag1 and mag2
    # t1w                = create_key('sub-{subject}/anat/sub-{subject}_T1w')
    # faces_bold         = create_key('sub-{subject}/func/sub-{subject}_task-faces_bold')
    # faces_sbref        = create_key('sub-{subject}/func/sub-{subject}_task-faces_sbref')
    # hipp_bold  = create_key('sub-{subject}/func/sub-{subject}_task-hipp_bold')
    # hipp_sbref = create_key('sub-{subject}/func/sub-{subject}_task-hipp_sbref')
    # rest_bold          = create_key('sub-{subject}/func/sub-{subject}_task-rest_bold')
    # rest_sbref         = create_key('sub-{subject}/func/sub-{subject}_task-rest_sbref')
    # fmap_taskMag       = create_key('sub-{subject}/fmap/sub-{subject}_acq-task_magnitude')
    # fmap_taskPhase     = create_key('sub-{subject}/fmap/sub-{subject}_acq-task_phasediff')
    # fmap_restMag       = create_key('sub-{subject}/fmap/sub-{subject}_acq-rest_magnitude')
    # fmap_restPhase     = create_key('sub-{subject}/fmap/sub-{subject}_acq-rest_phasediff')


    t1w                = create_key('sub-{subject}/anat/sub-{subject}_T1w')
    faces_bold         = create_key('sub-{subject}/func/sub-{subject}_task-faces_bold')
    faces_sbref        = create_key('sub-{subject}/func/sub-{subject}_task-faces_sbref')
    hipp_bold          = create_key('sub-{subject}/func/sub-{subject}_task-hipp_bold')
    hipp_sbref         = create_key('sub-{subject}/func/sub-{subject}_task-hipp_sbref')
    rest_bold          = create_key('sub-{subject}/func/sub-{subject}_task-rest_bold')
    rest_sbref         = create_key('sub-{subject}/func/sub-{subject}_task-rest_sbref')
    fmap_taskMag       = create_key('sub-{subject}/fmap/sub-{subject}_acq-task_magnitude')
    fmap_taskPhase     = create_key('sub-{subject}/fmap/sub-{subject}_acq-task_phasediff')
    fmap_restMag       = create_key('sub-{subject}/fmap/sub-{subject}_acq-rest_magnitude')
    fmap_restPhase     = create_key('sub-{subject}/fmap/sub-{subject}_acq-rest_phasediff')
    asl_nobiascorr     = create_key('sub-{subject}/asl/sub-{subject}_rec-nobiasfieldcorrection_asl')
    asl_biascorr       = create_key('sub-{subject}/asl/sub-{subject}_rec-biasfieldcorrection_asl')
    asl_PW             = create_key('sub-{subject}/asl_deriv/sub-{subject}_rec-PCASL_PW_asl')
    asl_relCBF         = create_key('sub-{subject}/asl_deriv/sub-{subject}_rec-PCASL_relCBF_asl')

    # h2mri-update: edit the below to match the file type variables create above
    info = {
                    t1w: [],
                    faces_bold: [],
                    faces_sbref: [],
                    hipp_bold: [],
                    hipp_sbref: [],
                    rest_bold: [],
                    rest_sbref: [],
                    fmap_taskMag: [],
                    fmap_taskPhase: [],
                    fmap_restMag: [],
                    fmap_restPhase: [],
                    asl_nobiascorr: [],
                    asl_biascorr: [],
                    asl_PW: [],
                    asl_relCBF: [],
                }

    for s in seqinfo:
        """
        The namedtuple `s` contains the following fields:
        * total_files_till_now
        * example_dcm_file
        * series_id
        * dcm_dir_name
        * unspecified2
        * unspecified3
        * dim1
        * dim2
        * dim3
        * dim4
        * TR
        * TE
        * protocol_name
        * is_motion_corrected
        * is_derived
        * patient_id
        * study_description
        * referring_physician_name
        * series_description
        * image_type
        """

        # h2mri-update: use the file BIDSConvert_02_dicominfRef.tsv to update the below for your sequences
        # amend these to match the dicominfo.tsv column "protocol_name" and "series_id" if you need to
        # idenitfy a specific dicom series (e.g. to seperate the sbref image from the main sequence)

        # idenitfy the un-biasfield corrected T1 image. fmriprep does bias field correction
        if s.protocol_name == 't1_mpr_ax_1mm_iso_32ch_v2'and s.series_id == '6-t1_mpr_ax_1mm_iso_32ch_v2':
                    info[t1w] = [s.series_id]
        if s.protocol_name == 'bold_mbep2d_MB4P2_task_faces' and s.series_id == '13-bold_mbep2d_MB4P2_task_faces':
                    info[faces_bold] = [s.series_id]
        if s.protocol_name == 'bold_mbep2d_MB4P2_task_faces' and s.series_id == '12-bold_mbep2d_MB4P2_task_faces':
                    info[faces_sbref] = [s.series_id]
        if s.protocol_name == 'BOLD_2p4_MB6_P1_TE30_TR800_AP_hippocampal' and s.series_id == '17-BOLD_2p4_MB6_P1_TE30_TR800_AP_hippocampal':
                    info[hipp_bold] = [s.series_id]
        if s.protocol_name == 'BOLD_2p4_MB6_P1_TE30_TR800_AP_hippocampal' and s.series_id == '16-BOLD_2p4_MB6_P1_TE30_TR800_AP_hippocampal':
                    info[hipp_sbref] = [s.series_id]
        if s.protocol_name == 'RS_BOLD_2p5_MB3_P2_TE15-36-57_TR1600_PA' and s.series_id == '23-RS_BOLD_2p5_MB3_P2_TE15-36-57_TR1600_PA':
                    info[rest_bold] = [s.series_id]
        if s.protocol_name == 'RS_BOLD_2p5_MB3_P2_TE15-36-57_TR1600_PA' and s.series_id == '22-RS_BOLD_2p5_MB3_P2_TE15-36-57_TR1600_PA':
                    info[rest_sbref] = [s.series_id]
        if s.protocol_name == 'fieldmap_2p4mm_FoV_216x216x144' and s.series_id == '14-fieldmap_2p4mm_FoV_216x216x144':
                    info[fmap_taskMag] = [s.series_id]
        if s.protocol_name == 'fieldmap_2p4mm_FoV_216x216x144' and s.series_id == '15-fieldmap_2p4mm_FoV_216x216x144':
                    info[fmap_taskPhase] = [s.series_id]
        if s.protocol_name == 'fieldmap_2p5mm_FoV_210x210x150' and s.series_id == '24-fieldmap_2p5mm_FoV_210x210x150':
                    info[fmap_restMag] = [s.series_id]
        if s.protocol_name == 'fieldmap_2p5mm_FoV_210x210x150' and s.series_id == '25-fieldmap_2p5mm_FoV_210x210x150':
                    info[fmap_restPhase] = [s.series_id]
        if s.protocol_name == 'to_ep2d_PCASL'and s.series_id == '18-to_ep2d_PCASL':
                    info[asl_nobiascorr] = [s.series_id]
        if s.protocol_name == 'to_ep2d_PCASL' and s.series_id == '19-to_ep2d_PCASL':
                    info[asl_biascorr] = [s.series_id]
        if s.protocol_name == 'to_ep2d_PCASL' and s.series_id == '20-to_ep2d_PCASL':
                    info[asl_PW] = [s.series_id]
        if s.protocol_name == 'to_ep2d_PCASL' and s.series_id == '21-to_ep2d_PCASL':
                    info[asl_relCBF] = [s.series_id]
    return info
