#!/bin/bash

# BRC Experimental medicine neuroimaging support
# Questions please contact Dr. Marieke Martens


datFold=/vols/Scratch/brc_em/7DP/sourcedata; #data folder



for sub in 101; do
    #101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231

    # for each mask in the following list, run invwarp

for mask in CSF_mask WM_mask LeftPCC_23a+b+31a+b+c_50_bin LeftAffectACC_24a+25+33+32a_50_bin LeftCognitionACC_24b+c+32b_50_bin; do


echo "Doing invwarp subject ${sub}"

fsl_sub -q short.q invwarp -w ${datFold}/fmriprep/work/fmriprep_wf/single_subject_${sub}_wf/anat_preproc_wf/anat_norm_wf/_template_MNI152NLin6Asym/registration/ants_t1_to_mniComposite.h5 -o ${datFold}/rest_fmriprep_tedana/seed/sub-${sub}/rest/standard2highres_warp.nii.gz -r ${datFold}/BIDS/sub-${sub}/anat/${sub}_T1w.nii.gz

done
done
