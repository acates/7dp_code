#!/bin/bash



# BRC Experimental medicine neuroimaging support
# Questions please contact Dr. Marieke Martens


datFold=/vols/Scratch/brc_em/7DP/sourcedata; #data folder

maskFold=/vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/masks;

for sub in 103 223; do
#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231

# for each mask in the following list, run fslmeants

for mask in PCC_50_bin AngularGyrus_50_bin Precentral_Gyrus_50_bin Postcentral_Gyrus_50_bin; do
#WM_mask CSF_mask PCC_50_bin AngularGyrus_50_bin Precentral_Gyrus_50_bin Postcentral_Gyrus_50_bin Right_Amygdala_50_bin Left_Amygdala_50_bin Right_Hippocampus_50_bin Left_Hippocampus_50_bin LeftPCC_23a+b+31a+b+c_50_bin LeftAffectACC_24a+25+33+32a_50_bin LeftCognitionACC_24b+c+32b_50_bin  RPCC_23+31a+b+c_50_bin RAffectACC_24a+25+33+32a_50_bin RCognitionACC_24b+32b+c_50_bin LeftPCC_23+31a+b+c+29+30_50_bin RPCC_23+31a+b+c+29+30a+b_50_bin
echo "Doing subject ${sub} ${mask}"


fsl_sub -q short.q fslmeants -i ${datFold}/tedana_output/sub-${sub}/rest/dn_ts_OC.nii.gz -m ${maskFold}/${mask}/sub-${sub}_${mask}_EPI.nii.gz -o ${maskFold}/fslmeants/sub-${sub}_${mask}_TS.txt


done
done
