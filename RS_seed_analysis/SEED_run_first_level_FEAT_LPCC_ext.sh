#!/bin/bash
# This script copies and adepts a specified feat design-fsf file from subject S001 for the other subjects, and runs it
# by Marco Whitman

# adapted by BRC Experimental medicine neuroimaging support
# Questions please contact Dr. Marieke Martens

DESIGN=LPCC_ext

DATA=/vols/Scratch/brc_em/7DP/sourcedata #where your data is


#S001_volume_number=`fslval ${DATA}/S001/raw_func/S001_rawfunc.nii.gz dim4 | sed 's/ //g'`	# when you extract the number of volumes, automatically remove spaces!
P101_volume_number=`fslval ${DATA}/tedana_output/sub-101/rest/dn_ts_OC.nii.gz dim4 | sed 's/ //g'`
echo "Subject 101 has $P101_volume_number volumes."


# get the total number of voxels:
dim1=`fslval ${DATA}/tedana_output/sub-101/rest/dn_ts_OC.nii.gz dim1 | sed 's/ //g'`
dim2=`fslval ${DATA}/tedana_output/sub-101/rest/dn_ts_OC.nii.gz dim2 | sed 's/ //g'`
dim3=`fslval ${DATA}/tedana_output/sub-101/rest/dn_ts_OC.nii.gz dim3 | sed 's/ //g'`
total_vox_ref=`echo "$dim1 * $dim2 * $dim3 * ${P101_volume_number}" | bc`


for sub in 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231; do
	#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231

echo "Doing person ${sub}"

# copy fsf-file from subject 101 and replace subject ID:
	        #sed "s/P101/${sub}/g" ${GLMS}/P101/${DESIGN}/feat_setup.fsf>${GLMS}/${sub}/${sub}_Nback{DESIGN}/feat_setup_Nback.fsf ;
	        sed "s/sub-101/sub-${sub}/g" ${DATA}/seed_analysis/designs/101_${DESIGN}_designs.fsf>${DATA}/seed_analysis/designs/${sub}_${DESIGN}_designs.fsf;
         	# find out volume number of subject and use it to replace P101 volume number:
		nr_vols=`fslval ${DATA}/tedana_output/sub-${sub}/rest/dn_ts_OC.nii.gz dim4 | sed 's/ //g'`

		# replace number of volumes:
		refnum="set fmri(npts) ${P101_volume_number}"
		newnum="set fmri(npts) ${nr_vols}"
		sed -i "s/$refnum/$newnum/g" ${DATA}/seed_analysis/designs/${sub}_${DESIGN}_designs.fsf;

		# replace also number of voxels in total
		total_vox_new=`echo "$dim1 * $dim2 * $dim3 * ${nr_vols}" | bc`
		sed -i "s/$total_vox_ref/$total_vox_new/g" ${DATA}/seed_analysis/designs/${sub}_${DESIGN}_designs.fsf
		echo "new saving path is ${DATA}/seed_analysis/designs/${sub}_${DESIGN}_designs"
		echo "this is nr_vols ${nr_vols}"

		# just to check:
		echo "for person ${sub} $P101_volume_number becomes $nr_vols"
		#echo "$total_vox_ref becomes $total_vox_new"

		# RUN feat:
		# remove hashtag below to run feats automatically
	        feat ${DATA}/seed_analysis/designs/${sub}_${DESIGN}_designs.fsf


		# note: diff FILE1 FILE2 checks if two text files are identical or in which lines are differences

 done
