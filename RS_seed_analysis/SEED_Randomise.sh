#!/bin/bash

for cope in 1 2; do

echo "Doing cope ${cope}"

#create the directories first where you would like to save your output to
#mkdir /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise
#mkdir /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope1
#mkdir /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope2
#echo "I made the randomise and cope folders"

#copy in MNI152_2mm_brain mask into randomise folder
#cp -r /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask.nii.gz

#copy your filtered_func_data 4D files from your each of your flame12 seed analyses
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LAffectiveCC_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RAffectiveCC_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LCogCC_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LCogCC_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RCogCC_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RCogCC_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LPCC_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LPCC_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RPCC_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RPCC_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/Precentral_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Precentral_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/Angular_Gyrus_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Angular_Gyrus_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LHippocampus_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LHippocampus_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RHippocampus_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RHippocampus_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LAmygdala_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAmygdala_group_filtered_func_data.nii.gz
#cp /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RAmygdala_group.gfeat/cope${cope}.feat/filtered_func_data.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAmygdala_group_filtered_func_data.nii.gz

#run randomise for each seed

#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LAffectiveCC_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LAffectiveCC_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RAffectiveCC_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RAffectiveCC_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LCogCC_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LCogCC_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LCogCC_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LCogCC_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RCogCC_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RCogCC_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RCogCC_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RCogCC_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LPCC_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LPCC_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LPCC_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LPCC_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RPCC_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RPCC_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RPCC_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RPCC_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Precentral_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Precentral_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/Precentral_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/Precentral_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Angular_Gyrus_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Angular_Gyrus_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/Angular_Gyrus_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/Angular_Gyrus_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LHippocampus_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LHippocampus_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LHippocampus_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LHippocampus_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RHippocampus_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RHippocampus_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RHippocampus_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RHippocampus_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAmygdala_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAmygdala_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LAmygdala_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/LAmygdala_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp
#fsl_sub -q verylong.q randomise -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAmygdala_group_filtered_func_data.nii.gz -o /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAmygdala_cope${cope} -d /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RAmygdala_group.gfeat/design.mat -t /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/seed_output/gfeats/RAmygdala_group.gfeat/design.con -m /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/MNI152_T1_2mm_brain_mask -n 5000 -T --uncorrp


#for once randomise has finished

echo "LAffectiveCC"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat2.nii.gz -R

#cluster -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat1.nii.gz --oindex=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat1_oindex --olmax=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat1_osize -t 0.95
#cluster -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat2.nii.gz --oindex=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat2_oindex --olmax=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAffectiveCC_cope${cope}_tfce_corrp_tstat2_osize -t 0.95

echo "RAffectiveCC"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat2.nii.gz -R

#cluster -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat1.nii.gz --oindex=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat1_oindex --olmax=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat1_osize -t 0.95
#cluster -i /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat2.nii.gz --oindex=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat2_oindex --olmax=/vols/Scratch/brc_em//vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAffectiveCC_cope${cope}_tfce_corrp_tstat2_osize -t 0.95

echo "LCogCC"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LCogCC_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LCogCC_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "RCogCC"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RCogCC_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RCogCC_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "LPCC"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LPCC_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LPCC_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "RPCC"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RPCC_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RPCC_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "Precentral"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Precentral_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Precentral_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "Angular_Gyrus"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Angular_Gyrus_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/Angular_Gyrus_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "LHipp"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LHippocampus_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LHippocampus_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "RHipp"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RHippocampus_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RHippocampus_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "LAmygdala"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAmygdala_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/LAmygdala_cope${cope}_tfce_corrp_tstat2.nii.gz -R

echo "RAmygdala"
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAmygdala_cope${cope}_tfce_corrp_tstat1.nii.gz -R
fslstats /vols/Scratch/brc_em/7DP/sourcedata/seed_analysis/randomise/cope${cope}/RAmygdala_cope${cope}_tfce_corrp_tstat2.nii.gz -R

done
