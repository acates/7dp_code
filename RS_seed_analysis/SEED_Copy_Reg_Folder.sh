#!/bin/bash

# BRC Experimental medicine neuroimaging support
# Questions please contact Dr. Marieke Martens


datFold=/vols/Scratch/brc_em/7DP/sourcedata; #data folder
DESIGN=LAmygdala
#LACC LCogCC RACC RCogCC RPCC RPCC_ext LPCC LPCC_ext ACC PCC Precentral_Gyrus Postcentral_Gyrus Angular_Gyrus LHippocampus RHippocampus LAmygdala


for sub in 224; do
#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231

echo "Doing subject ${sub}"

#cd ${datFold}/seed_analysis/seed_output/sub-${sub}_${DESIGN}.feat
#mkdir reg
    # shouldn't need these two steps above
cp -R ${datFold}/rest_FSL/sub-${sub}_rest_fsl.ica/reg ${datFold}/seed_analysis/seed_output/sub-${sub}_${DESIGN}.feat/reg

done
