#!/bin/bash

# BRC Experimental medicine neuroimaging support
# Questions please contact Dr. Marieke Martens


datFold=/vols/Scratch/brc_em/7DP/sourcedata; #data folder



for sub in 101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231; do
    #101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231

    # for each mask in the following list, run invwarp

for mask in AngularGyrus_50_bin Precentral_Gyrus_50_bin Postcentral_Gyrus_50_bin ACC_50_bin PCC_50_bin; do
#WM_masks CSF_mask Right_Amygdala_50_bin Left_Amygdala_50_bin Right_Hippocampus_50_bin Left_Hippocampus_50_bin

echo "Doing invwarp subject ${sub}"

fsl_sub -q short.q invwarp -w ${datFold}/rest_FSL/sub-${sub}_rest_fsl.ica/reg/highres2standard_warp.nii.gz -o ${datFold}/rest_FSL/sub-${sub}_rest_fsl.ica/reg/standard2highres_warp.nii.gz -r ${datFold}/BIDS/sub-${sub}/anat/sub-${sub}_T1w.nii.gz

done
done
