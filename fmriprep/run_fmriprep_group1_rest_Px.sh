# run fmriprep
# https://fmriprep.readthedocs.io/en/stable/installation.html

# singularity image pulled 18/3/21 with:
# singularity pull --name singularityContainer_fmriprep-v20.2.0.simg docker://poldracklab/fmriprep:latest

# submitting job with the name "fmriprep" to the bigmem queue
# the first two lines bind the root ("base") (line 21) to make available to the singularity container (line 22)
# next two lines specify the "data" (BIDS) (line 23) and the "output" directories for the qc results (line 24)
# "participant" specifies the qc type - all participants listed will be processed (line 25)
# the "--participant-label" flag takes a list of pxIDs for preprocessing, expected to be in the BIDS directory

# h2mri: Update the bind path for "base" and the location of the singularityContainer
# h2mri: ensure the "data and "output" directories are correct for your file structure
# h2mri: ensure the particpant id is correct for participants you would like to preprocess in your BIDS directory
# h2mri: ensure the task-id is correct for the task data you would like to preprocces, or leave this flag out if you would like to proeprocess all data in your BIDS directory.
# h2mri: ensure the fs-licence-file is pointing to your code folder


#  FROM CASS: Running with fs for a different participant and only singe echo data incase it is the multiecho


    #fsl_sub -N fmriprep -q bigmem.q \
    #ran same script on long queue with additional allocation of memory (2*12MB) - 104 done as pilot
    fsl_sub -N fmriprep -q long.q -s openmp,4 \
    singularity run --cleanenv -B \
        /vols/Scratch/brc_em/7DP/:/base \
        /vols/Scratch/brc_em/7DP/code_AdeC/fmriprep/fmriprep/singularityContainer_fmriprep-v20.2.0.simg \
        /base/sourcedata/BIDS \
        /base/sourcedata/fmriprep/fmriprep \
        participant \
        -w /base/sourcedata/fmriprep/work \
        --fs-license-file /base/code_AdeC/fmriprep/fmriprep/license.txt \
        --participant-label 103 104 105 106 107 \
        --output-spaces MNI152NLin6Asym:res-2 \
        --ignore slicetiming \
        --fs-no-reconall \
        --omp-nthreads 1 \
        --n_cpus 8 \
        --skull-strip-fixed-seed \
        --random-seed 999 \
        --bold2t1w-dof 12 --force-bbr \
        --task-id rest \
        --write-graph
