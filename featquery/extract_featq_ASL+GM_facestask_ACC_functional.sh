#!/bin/bash

# This script runs a series of feat queries!

base_dir='/vols/Scratch/brc_em/7DP/sourcedata/feat/task-faces/feat_first-level_FSL_firstrun' #change path for controls
group_name='healthyvolunteers' # or Controls - will be appended to summary file filename
save_path='/vols/Scratch/brc_em/7DP/sourcedata/feat/task-faces/featqueryResults/'


cd "$base_dir"

# read out a list of subjects to run, from subject_list.txt
#cat $base_dir/subject_list.txt | while read subject
#101 102-exc 103 104 105 106 107 108 109 110 111 112 113 115-exc 116 117 118 119 122 123 124 125 127-exc 201 203 204 205 206 207 209 210 211 212 213 214-exc 216 217 218 219 220 221 222 223 224 225 227-exc 228 230 231 232-exc
sub_list=(101 103 104 105 106 107 108 109 110 111 112 113 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231)
mask_list=(meangrtbase_A+B_functionalmask_ACC_bin)
cope_list=(1 2 3 4 5 6 7 8 9)

# Make files to save results
output_file=Fq_Res_${group_name} #{contr_num}
echo filename $output_file

# 1st header line: name of the masks
printf "name\t" > ${save_path}${output_file}_mean.txt
for cmask in ${mask_list[@]}; do
 for ccope in ${cope_list[@]}; do
     printf  "${cmask}\t" >> ${save_path}${output_file}_mean.txt
 done
done

printf  "\n\t" >> ${save_path}${output_file}_mean.txt
# 2nd header line: name of mask
for cmask in ${mask_list[@]}; do
 for ccope in ${cope_list[@]}; do
     printf  "${ccope}\t" >> ${save_path}${output_file}_mean.txt
 done
done

# Write participant name
for csub in ${sub_list[@]} ; do
  printf "\n" >> ${save_path}${output_file}_mean.txt
  printf "${csub}" >> ${save_path}${output_file}_mean.txt

# Open featquery file and extract numbers
 for cmask in ${mask_list[@]};do

  target_file=${base_dir}/sub-${csub}_faces_fsl.feat/featquery_${cmask}/report.txt

  # Check whether the target file exists
   if [ -f $target_file ]
   then
     vartemp='1' # echo File is ${target_file}
   else
     target_file=${base_dir}/sub-${csub}_faces_fsl.feat/featquery_${cmask}+/report.txt
   fi

  result_1_mean=`cat ${target_file} | grep stats/cope | awk '{print $6}'`
  printf "\t%2.4f" ${result_1_mean} >> ${save_path}${output_file}_mean.txt
 done
done
