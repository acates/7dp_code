#!/bin/bash

# This script runs a series of feat queries!

base_dir='/vols/Scratch/brc_em/7DP/sourcedata/feat/task-hipp_fsl/feat_firstlevels_FSL_firstrun'
#main_dir='home/lilianac/Desktop/capitao/patients'
mask_dir='/vols/Scratch/brc_em/7DP/masks/'

cd "$base_dir"

# read out a list of subjects to run, from subject_list.txt
#cat $base_dir/subject_list.txt | while read subject
#101 102-exc 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 127-exc 201 203-exc 204 205 206 207 209 210 211 212 213 214-exc 216 217 218 219 220 221 222 223 224 225 227-exc 228 230 231 232-exc
sub_list=(101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231)

#for each subject
for csub in ${sub_list[@]} ; do

# for each mask in the following list, run the query
for mask in WBHippFSLrun1_cope2_cluster_mask_zstat2_bin

  #.nii.gz will automatically be appended to the mask name below

# other masks:

do

echo I am about to try subject "$csub" with mask "$mask" wish me luck
#the actual query. runs all 10 pe's and 5 cope's. names the output directory after $mask.${csub}


featquery 1 ${base_dir}/sub-${csub}_hipp_fsl.feat 4 stats/cope1 stats/cope2 stats/cope3 stats/cope4 featquery_${mask} -p ${mask_dir}/${mask}.nii.gz


# echo what happened.
echo I just did person ${csub} with mask ${mask} will be saved in featquery_${mask}

done

done
