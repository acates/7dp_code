# run mriqc
# https://mriqc.readthedocs.io/en/stable/docker.html?highlight=docker#explaining-the-mriqc-docker-command-line

# singularity image pulled 12/08/19 with:
# singularity pull --name singularityContainer_mriqc_0152rc1.sif docker://poldracklab/mriqc:latest

# from code folder in terminal, you can verify container with below to get version number (0.15.2rc1):
# singularity run singularityContainer_mriqc_0152rc1.sif --version

# first line submits the job to the long queue (line 18).
# next two lines bind the root ("base") (line 20) to make available to the singularity container (line 21)
# next two lines specify the "data" (BIDS) (line 22) and the "output" directories for the qc results (line 23)
# "participant" specifies the qc type - all participants in directory will be processed and then group stats run as have not passed "--participant_label" (line 24)

# h2mri: Update the bind path foßr "base" and the location of the singularityContainer
# h2mri: ensure the "data and "output" directories are correct for your file structure

fsl_sub -q bigmem.q \
singularity run --cleanenv -B \
    /vols/Scratch/brc_em/7DP/:/base \
    /vols/Scratch/brc_em/7DP/code_AdeC/mriqc/singularityContainer_mriqc_0152rc1.sif \
    /base/sourcedata/BIDS \
    /base/code_AdeC/mriqc/rest_echo_2 \
    participant \
    --task-id rest_echo-2
