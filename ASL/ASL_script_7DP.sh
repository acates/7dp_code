#!/bin/bash

#for each subject
#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231; do
for sub in 101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231; do

#The first command (below) takes the first volume (vol 0 - the calibration image) from the main image file and
#places it in a separate image file called 'calib_image'.

echo "Doing person ${sub}"

echo “doing create calib_image”

fslroi /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_rec-biasfieldcorrection_asl.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_calib_image 0 1

#The next command (below) takes the next 97 volumes (the asl tag-control pairs) from the main image file and
#places them in a separate image file called 'asl_data'.

echo “doing create asl_data image”

fslroi /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_rec-biasfieldcorrection_asl.nii.gz /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_asl_data 1 96

#Then run fsl_anat on the high resolution T1 weighted structural image so that we can use the fslanat output
#directory as an input to oxford_asl

#echo “doing FSL Anat”
#fsl_sub -q short.q fsl_anat -o /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/sub-${sub}_FSL_anat -i /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/anat/sub-${sub}_T1w.nii.gz


echo “doing Oxford_ASL”
#this is the version with the fmaps
fsl_sub -q long.q oxford_asl -i /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_asl_data -o /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_ASL_Output --iaf=tc --ibf=rpt --casl --bolus=1.4 --slicedt=0.0452 --tis=1.65,1.9,2.15,2.4,2.65,2.9 --fixbolus --mc -c /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_calib_image --cmethod=voxel --tr=6 --fslanat=/vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/sub-${sub}_FSL_anat.anat --pvcorr --fmap=/vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/fmap/sub-${sub}_phase-rads.nii.gz --fmapmag=/vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/fmap/sub-${sub}_acq-task_magnitude1.nii.gz --fmapmagbrain=/vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/fmap/sub-${sub}_fieldmap-masked.nii.gz --pedir=-y --echospacing=0.00049
#fsl_sub -q long.q oxford_asl -i /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_asl_data -o /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_ASL_Output --iaf=tc --ibf=rpt --casl --bolus=1.4 --slicedt=0.0452 --tis=1.65,1.9,2.15,2.4,2.65,2.9 --fixbolus --mc -c /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/asl/sub-${sub}_calib_image --cmethod=voxel --tr=6 --fslanat=/vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${sub}/sub-${sub}_FSL_anat.anat --pvcorr --pedir=-y --echospacing=0.00049





done
