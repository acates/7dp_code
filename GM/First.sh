for ID in 101; do
#101 102-exc 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 127-exc 201 203 204 205 206 207 209 210 211 212 213 214-exc 216 217 218 219 220 221 222 223 224 225 227-exc 228 230 231 232-exc

#Run FIRST to segment L+R hippocampus for each participant
#run_first_all -i /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1w.nii.gz -s L_Hipp,R_Hipp -o /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_FIRST

#Create R hipp mask
#fsl_sub -q veryshort.q fslmaths /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_FIRST_all_fast_firstseg.nii.gz -thr 53 -uthr 53 -bin  /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_R_hipp_bin

#Create L hipp mask
#fsl_sub -q veryshort.q fslmaths /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_FIRST_all_fast_firstseg.nii.gz -thr 17 -uthr 17 -bin  /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_L_hipp_bin

#Run featquery
#fsl_sub -q short.q featquery 1 /vols/Scratch/brc_em/7DP/sourcedata/feat/task-hipp_fsl/feat_firstlevels_FSL_firstrun/sub-${ID}_hipp_fsl.feat 4 stats/cope1 stats/cope2 stats/cope3 stats/cope4 featquery_FIRST_R_hipp -p /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_R_hipp_bin.nii.gz

#fsl_sub -q short.q featquery 1 /vols/Scratch/brc_em/7DP/sourcedata/feat/task-hipp_fsl/feat_firstlevels_FSL_firstrun/sub-${ID}_hipp_fsl.feat 4 stats/cope1 stats/cope2 stats/cope3 stats/cope4 featquery_FIRST_L_hipp -p /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_L_hipp_bin.nii.gz


#fsl_sub -q veryshort.q fslmaths /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_FIRST_all_fast_firstseg.nii.gz -thr 53 -uthr 53 /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_R_hipp

#fsl_sub -q veryshort.q fslmaths /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_FIRST_all_fast_firstseg.nii.gz -thr 17 -uthr 17 /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_L_hipp

echo "doing sub-${ID}"

fslstats /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_R_hipp -V

fslstats /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_T1_subcort_seg_L_hipp -V

fslstats /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-${ID}/anat/sub-${ID}_structural_brain.nii.gz -V


done
