import os
import subprocess
import shlex
import shutil


# Set paths

studydir = '/vols/Scratch/brc_em/7DP'
datadir = os.path.join(studydir,'sourcedata','tedana_output')


# Set participants

#subjects = ['101','103','104','105','106','107','108','109','110','111','112','113','115','116','117','118','119','122','123','124','125','201','204','205','206','207','209','210','211','212','213','216','217','218','219','220','221','222','223','225','228','230','231']

subjects = ['224']


# ### Apply transform for faces task using ants

for sub in subjects:
    try:
        print(f'applying transform for sub {sub}')
        print(datadir)
        input_file = os.path.join(datadir,f'sub-{sub}/rest/dn_ts_OC.nii.gz')
        print(input_file)
        #input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')
        output_file = os.path.join(studydir,'sourcedata','fmriprep','fmriprep','fmriprep',f'sub-{sub}/func/sub-{sub}_task-rest_space-MNI152NLin6Asym_res-2_desc-dn_ts_OC_transformed.nii.gz')
        reference_image = os.path.join(studydir,'sourcedata','fmriprep','fmriprep','template','tpl-MNI152NLin6Asym_res-02_T1w.nii.gz')
        t1_2_mni_transform = os.path.join(studydir,'sourcedata','fmriprep','work','fmriprep_wf',f'single_subject_{sub}_wf/anat_preproc_wf/anat_norm_wf/_template_MNI152NLin6Asym/registration/ants_t1_to_mniComposite.h5')
        bold_2_t1_transform = os.path.join(studydir,'sourcedata','fmriprep','work','fmriprep_wf',f'single_subject_{sub}_wf/func_preproc_task_rest_echo_1_wf/bold_reg_wf/fsl_bbr_wf/fsl2itk_fwd/affine.txt')
        sdc_correction_transform = os.path.join(studydir,'sourcedata','fmriprep','work','fmriprep_wf',f'single_subject_{sub}_wf/func_preproc_task_rest_echo_1_wf/sdc_estimate_wf/fmap2field_wf/vsm2dfm/sub-{sub}_acq-rest_phasediff_rads_unwrapped_recentered_filt_demean_maths_fmap_trans_rad_vsm_unmasked_desc-field_sdcwarp.nii.gz')

        if os.path.isfile(input_file):
            print('input file exists')
        else:
            print('input file does not exist')

        if os.path.isfile(reference_image):
            print('reference file exists')
        else:
            print('reference file does not exist')

        if os.path.isfile(t1_2_mni_transform):
            print('t1_2_mni file exists')
        else:
            print('t1_2_mni file does not exist')

        if os.path.isfile(bold_2_t1_transform):
            print('bold_2_t1 file exists')
        else:
            print('bold_2_t1 file does not exist')

        if os.path.isfile(sdc_correction_transform):
            print('sdc correction file exists')
        else:
            print('sdc correction file does not exist')

        subprocess.run('antsApplyTransforms')
        run_applytransform = shlex.split(f'fsl_sub -q short.q antsApplyTransforms --default-value 0 --input-image-type 3 --dimensionality 3 --float 0 --input {input_file} --interpolation LanczosWindowedSinc --output {output_file} --reference-image {reference_image} --transform {t1_2_mni_transform} --transform {bold_2_t1_transform} --transform {sdc_correction_transform}')
        completed = subprocess.run(
            run_applytransform,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)
    else:
        print('returncode:', completed.returncode)
        print(completed)
        input('\n ** PRESS ANY KEY TO CONTINUE (ctrl+C to quit) **')
