#!/bin/bash
#must run this code from within appropriate sourcedata folder (split) so put another copy in that folder and run from there

datFold=/vols/Scratch/brc_em/7DP/sourcedata/rest_fmriprep_tedana/Dual_Regression_d20; #data folder

for nb in 0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 0010 0011 0012 0013 0014 0015 0016 0017 0018 0019; do
#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231

echo "Doing component ${nb}"

fslmerge -t ${datFold}/SM/ANALYSIS/Comp${nb} sub-104_Comp${nb}.nii.gz sub-105_Comp${nb}.nii.gz sub-108_Comp${nb}.nii.gz sub-110_Comp${nb}.nii.gz sub-112_Comp${nb}.nii.gz sub-113_Comp${nb}.nii.gz sub-117_Comp${nb}.nii.gz sub-118_Comp${nb}.nii.gz sub-123_Comp${nb}.nii.gz sub-124_Comp${nb}.nii.gz sub-204_Comp${nb}.nii.gz sub-206_Comp${nb}.nii.gz sub-209_Comp${nb}.nii.gz sub-212_Comp${nb}.nii.gz sub-216_Comp${nb}.nii.gz sub-218_Comp${nb}.nii.gz sub-220_Comp${nb}.nii.gz sub-221_Comp${nb}.nii.gz sub-224_Comp${nb}.nii.gz sub-230_Comp${nb}.nii.gz sub-231_Comp${nb}.nii.gz sub-101_Comp${nb}.nii.gz sub-103_Comp${nb}.nii.gz sub-106_Comp${nb}.nii.gz sub-107_Comp${nb}.nii.gz sub-109_Comp${nb}.nii.gz sub-111_Comp${nb}.nii.gz sub-115_Comp${nb}.nii.gz sub-116_Comp${nb}.nii.gz sub-119_Comp${nb}.nii.gz sub-122_Comp${nb}.nii.gz sub-125_Comp${nb}.nii.gz sub-201_Comp${nb}.nii.gz sub-205_Comp${nb}.nii.gz sub-207_Comp${nb}.nii.gz sub-210_Comp${nb}.nii.gz sub-211_Comp${nb}.nii.gz sub-213_Comp${nb}.nii.gz sub-217_Comp${nb}.nii.gz sub-219_Comp${nb}.nii.gz sub-222_Comp${nb}.nii.gz sub-223_Comp${nb}.nii.gz sub-225_Comp${nb}.nii.gz sub-228_Comp${nb}.nii.gz

done
