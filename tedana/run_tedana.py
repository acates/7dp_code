import os
import subprocess
import shlex
import shutil



## Set relevant paths
studydir = '/vols/Scratch/brc_em/7DP'
datadir = os.path.join(studydir,'sourcedata','fmriprep','fmriprep', 'fmriprep')


# Set participant list
# subjects = ['101','103','104','105','106','107','108','109','110','111','112','113','115','116','117','118','119','122','123','124','125','201','203','204','205','206','207','209','210','211','212','213','214','216','217','218','219','220','221','222','223','224','225','228','230','231']
subjects = ['224']



### Run tedana for rest fmri

#Create manual mask (only need to use echo 1 as this has the best signal) for processing using bet
for sub in subjects:
    print(f'Create echo 1 mask for participant {sub}')
    echo1_image = f'{datadir}/sub-{sub}/func/sub-{sub}_task-rest_echo-1_space-native_desc-partialPreproc_bold.nii.gz'
    bet_command = shlex.split(f'fsl_sub -q short.q bet {echo1_image} {datadir}/sub-{sub}/func/sub-{sub}_task-rest_echo-1_space-native_tedana_mask_bet.nii.gz -m -n')
    subprocess.run(bet_command)


#Extract first mask volume; discard the rest
for sub in subjects:
    print(f'Extract first mask volume for participant {sub}')
    echo1_image_mask = f'{datadir}/sub-{sub}/func/sub-{sub}_task-rest_echo-1_space-native_tedana_mask_bet_mask.nii.gz'
    bet_command = shlex.split(f'fsl_sub -q veryshort.q fslroi {echo1_image_mask} {datadir}/sub-{sub}/func/sub-{sub}_task-rest_echo-1_space-native_tedana_mask_bet_mask.nii.gz 0 1')
    subprocess.run(bet_command)

## Run tedana
for sub in subjects:
    if not os.path.isdir(os.path.join(studydir, f'sourcedata/tedana_output/sub-{sub}/rest')):
        os.makedirs(os.path.join(studydir, f'sourcedata/tedana_output/sub-{sub}/rest'))
    try:
        echo1_file = os.path.join(datadir,f'sub-{sub}/func/sub-{sub}_task-rest_echo-1_space-native_desc-partialPreproc_bold.nii.gz')
        echo2_file = os.path.join(datadir,f'sub-{sub}/func/sub-{sub}_task-rest_echo-2_space-native_desc-partialPreproc_bold.nii.gz')
        echo3_file = os.path.join(datadir,f'sub-{sub}/func/sub-{sub}_task-rest_echo-3_space-native_desc-partialPreproc_bold.nii.gz')
        mask_img = os.path.join(datadir,f'sub-{sub}/func/sub-{sub}_task-rest_echo-1_space-native_tedana_mask_bet_mask.nii.gz')
        output_dir = f'/vols/Scratch/brc_em/7DP/sourcedata/tedana_output/sub-{sub}/rest'

        run_tedana = shlex.split(f'fsl_sub -s openmp,6 -q short.q tedana -d {echo1_file} {echo2_file} {echo3_file} -e 15 36.19 57.38 --mask {mask_img} --tedpca mdl --seed 10 --maxit 5000 --png-cmap "coolwarm" --out-dir {output_dir}')
        completed = subprocess.run(
            run_tedana,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)
    else:
        print('returncode:', completed.returncode)
