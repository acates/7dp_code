#!/bin/bash


datFold=/vols/Scratch/brc_em/7DP/sourcedata; #data folder

for ID in 101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231; do
#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231


fsl_sub -q short.q fsl_glm -i ${datFold}/fmriprep/fmriprep/fmriprep/sub-${ID}/func/sub-${ID}_task-rest_space-MNI152NLin6Asym_res-2_desc-dn_ts_OC_transformed_smoothed.nii.gz -d ${datFold}/rest_fmriprep_tedana/Group_Output_20.gica/melodic_IC.nii.gz -m /vols/Scratch/brc_em/7DP/code_AdeC/tedana/tpl-MNI152NLin6Asym_res-02_desc-brain_mask.nii.gz -o ${datFold}/rest_fmriprep_tedana/Dual_Regression_d20/TS/sub-${ID}_TS.txt --demean

done
