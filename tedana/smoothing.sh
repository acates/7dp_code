#!/bin/bash
# This script smooths tedana output that has just been transformed into standard space

datFold=/vols/Scratch/brc_em/7DP/sourcedata/fmriprep/fmriprep/fmriprep; #data folder

for ID in 224; do
#101 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 201 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231


fsl_sub fslmaths ${datFold}/sub-${ID}/func/sub-${ID}_task-rest_space-MNI152NLin6Asym_res-2_desc-dn_ts_OC_transformed.nii.gz -s 3 ${datFold}/sub-${ID}/func/sub-${ID}_task-rest_space-MNI152NLin6Asym_res-2_desc-dn_ts_OC_transformed_smoothed.nii.gz

done
