#!/bin/bash


datFold=/vols/Scratch/brc_em/7DP/sourcedata/rest_fmriprep_tedana; #data folder

for nb in 0016; do
#0000 0001 0002 0003 0004 0005 0006 0007 0008 0009 0010 0011 0012 0013 0014 0015 0016 0017 0018 0019 0020 0021 0022 0023 0024

fsl_sub -q verylong.q randomise -i ${datFold}/Dual_Regression_d20/SM/ANALYSIS/Comp${nb}.nii.gz -o ${datFold}/Randomise_d20/Randomise_results_Smith/Comp${nb}_OUTPUT -m /vols/Scratch/brc_em/7DP/sourcedata/rest_fmriprep_tedana/Smith09_networks0007_bin_3.nii.gz -d ${datFold}/Randomise_d25/randomise_minusASL+GM_design.mat -t ${datFold}/Randomise_d25/randomise_minusASL+GM_design.con -n 5000 -T -c 3.1 --uncorrp

done
