This code is for the analysis of the Seven Day Prucalopride study (The effect of seven day prucalopride administration on emotional processing in healthy volunteers). Thanks to all who contributed, especially Cassandra Gould van Praag

Noticed any problems, contact: Angharad de Cates angharad.decates@psych.ox.ac.uk; https://www.psych.ox.ac.uk/team/angharad-de-cates

Please cite this material using the doi: 10.5281/zenodo.6354716
