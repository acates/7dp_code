#!/bin/bash

fsl_sub -q short.q

base_dir='/vols/Scratch/brc_em/7DP/sourcedata/BIDS'

#for each subject - 101 102 103 104 105 106 107 108 109 110 111 112 113 115-exc 116 117 118 119 122 123 124 125 127-exc 201 203 204 205 206 207 209 210 211 212 213 214-exc 216 217 218 219 220 221 222 223 224 225 227-exc 228 230 231 232-exc
for ID in 101 102 103 104 105 106 107 108 109 110 111 112 113 116 117 118 119 122 123 124 125 201 203 204 205 206 207 209 210 211 212 213 216 217 218 219 220 221 222 223 224 225 228 230 231; do

echo "----Doing person ${ID}"

echo “----doing flirt”
#register the mag fieldmap to the structural (non betted) using FLIRT
flirt -in ${base_dir}/sub-${ID}/fmap/sub-${ID}_acq-task_magnitude1.nii.gz -ref ${base_dir}/sub-${ID}/anat/sub-${ID}_T1w.nii.gz -omat ${base_dir}/sub-${ID}/fmap/sub-${ID}_Affinematrix.mat

echo “----invert”
#invert the matrix from registration above, this inverted matrix is for structural to fieldmap
convert_xfm -omat ${base_dir}/sub-${ID}/fmap/sub-${ID}_inverse-matrix.mat -inverse ${base_dir}/sub-${ID}/fmap/sub-${ID}_Affinematrix.mat

echo “----apply_mask”
#applies mask created above to fieldmap mag to get final image
flirt -interp nearestneighbour -in ${base_dir}/sub-${ID}/anat/sub-${ID}_structural_brain_mask.nii.gz -ref ${base_dir}/sub-${ID}/fmap/sub-${ID}_acq-task_magnitude1.nii.gz -applyxfm -init ${base_dir}/sub-${ID}/fmap/sub-${ID}_inverse-matrix.mat -out ${base_dir}/sub-${ID}/fmap/sub-${ID}_fieldmap-flirt

echo “----fslmaths”
#applies mask created above to fieldmap mag to get final image
fslmaths ${base_dir}/sub-${ID}/fmap/sub-${ID}_acq-task_magnitude1.nii.gz -mas ${base_dir}/sub-${ID}/fmap/sub-${ID}_fieldmap-flirt ${base_dir}/sub-${ID}/fmap/sub-${ID}_fieldmap-masked


done
