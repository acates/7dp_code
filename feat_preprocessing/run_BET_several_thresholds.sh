#!/bin/bash

Data=/vols/Scratch/brc_em/7DP/sourcedata/BIDS

for ID in 101 102 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 127 201 203 204 205 206 207 209 210 211 212 213 214 216 217 218 219 220 221 222 223 224 225 227 228 230 231 232  ; do

# 101 102 103 104 105 106 107 108 109 110 111 112 113 115 116 117 118 119 122 123 124 125 127 201 203 204 205 206 207 209 210 211 212 213 214 216 217 218 219 220 221 222 223 224 225 227 228 230 231 232

for f in 20 25 30 35 40 45 50 60 70 75 80; do #Different mask settings 20 25 30 35 40 45 50 60 70 75 80 - don’t change this

echo "ID $ID mask: $f"

#Run bet on the structural
/opt/fmrib/fsl/bin/bet ${Data}/sub-${ID}/anat/sub-${ID}_T1w.nii.gz ${Data}/sub-${ID}/anat/sub-${ID}_structural_brain${f} -f 0.${f} -m


# -f 0.${f} = try out different thresholds (from 0.20 to 0.80 atm)
# -m generates a binary brain mask

done
done
