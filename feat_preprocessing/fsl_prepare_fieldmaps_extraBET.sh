#!/bin/bash
# This script runs prepare fieldmaps

datFold=/vols/Scratch/brc_em/7DP/sourcedata/BIDS; #data folder

for ID in 105 117 ; do
#101 102 103 104 105 106 107 108 109 110 111 112 113 115-exc 116 117 118 119 122 123 124 125 127-exc 201 203 204 205 206 207 209 210 211 212 213 214-exc 216 217 218 219 220 221 222 223 224 225 227-exc 228 230 231 232-exc


fsl_prepare_fieldmap SIEMENS ${datFold}/sub-${ID}/fmap/sub-${ID}_acq-rest_phasediff.nii.gz ${datFold}/sub-${ID}/fmap/sub-${ID}_acq-rest_magnitude1_brain_edited-eroded.nii.gz ${datFold}/sub-${ID}/fmap/sub-${ID}_phase-rads.nii.gz 2.46

done
