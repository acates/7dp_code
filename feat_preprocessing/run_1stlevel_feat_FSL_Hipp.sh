#!/bin/bash
# This script copies and adepts a specified feat design-fsf file from subject S001 for the other subjects, and runs it
# by Marco Whitman

DESIGN=hipp1a_extra_allimages

DATA=/vols/Scratch/brc_em/7DP/sourcedata/BIDS #where the persons you want to make the file for are


#S001_volume_number=`fslval ${DATA}/S001/raw_func/S001_rawfunc.nii.gz dim4 | sed 's/ //g'`	# when you extract the number of volumes, automatically remove spaces!
P102_volume_number=`fslval ${DATA}/sub-101/func/sub-101_task-hipp_bold.nii.gz   dim4 | sed 's/ //g'`
echo "Subject 101 has $P102_volume_number volumes."


# get the total number of voxels:
dim1=`fslval ${DATA}/sub-101/func/sub-101_task-hipp_bold.nii.gz  dim1 | sed 's/ //g'`
dim2=`fslval ${DATA}/sub-101/func/sub-101_task-hipp_bold.nii.gz  dim2 | sed 's/ //g'`
dim3=`fslval ${DATA}/sub-101/func/sub-101_task-hipp_bold.nii.gz  dim3 | sed 's/ //g'`
total_vox_ref=`echo "$dim1 * $dim2 * $dim3 * ${P102_volume_number}" | bc`

#101 103 104 105 106 107 108 109 110 111 112(eroded) 113 115-exc 116 117(eroded) 118 119 122 123 124 125 127-exc 201 203 204 205 206 207 209 210 211 212 213 214-exc 216 217 218 219 220 221 222 223 224 225 227-exc 228 230 231 232-exc

for ID in 104; do
    echo "Doing person ${ID}"
	if [ ! -f "${DATA}/sub-${ID}/anat/sub-${ID}_structural_brain.nii.gz" ]      	 	# 2. check if betted image is available
	then
	        echo "checking ${DATA}/sub-${ID}/anat/sub-${ID}_structural.nii.gz"
		echo "${ID} has no betted brain image!"
	elif [ ! -f "${DATA}/sub-${ID}/anat/sub-${ID}_structural.nii.gz" ]     	     	#4. check if unbetted brain is available
	then
		echo "${sub} has no raw structural brain image!"
	else
		# copy fsf-file from subject P102 and replace subject ID:
	        #sed "s/P101/${sub}/g" ${GLMS}/P101/${DESIGN}/feat_setup.fsf>${GLMS}/${sub}/${DESIGN}/feat_setup.fsf ;

	        sed "s/101/${ID}/g" /vols/Scratch/brc_em/7DP/sourcedata/BIDS/sub-101/designs/feat${DESIGN}_setup.fsf>${DATA}/sub-${ID}/designs/feat${DESIGN}_setup.fsf ;
         	# find out volume number of subject and use it to replace S001 volume number:
		nr_vols=`fslval ${DATA}/sub-${ID}/func/sub-${ID}_task-hipp_bold.nii.gz dim4 | sed 's/ //g'`

		# replace number of volumes:
		refnum="set fmri(npts) ${P102_volume_number}"
		newnum="set fmri(npts) ${nr_vols}"
		sed -i "s/$refnum/$newnum/g" ${DATA}/sub-${ID}/designs/feat${DESIGN}_setup.fsf ;

		# replace also number of voxels in total
		total_vox_new=`echo "$dim1 * $dim2 * $dim3 * ${nr_vols}" | bc`
		sed -i "s/$total_vox_ref/$total_vox_new/g" ${DATA}/sub-${ID}/designs/feat${DESIGN}_setup.fsf
		echo  "new saving path is ${DATA}/sub-${ID}/designs/feat${DESIGN}_setup.fsf"
		echo "this is nr_vols ${nr_vols}"

		# just to check:
		echo "for person ${sub} $P102_volume_number becomes $nr_vols"
		#echo "$total_vox_ref becomes $total_vox_new"

		# RUN feat:
		# remove hashtag below to run feats automatically
	  fsl_sub -q long.q feat ${DATA}/sub-${ID}/designs/feat${DESIGN}_setup.fsf


		# note: diff FILE1 FILE2 checks if two text files are identical or in which lines are differences

	fi
 done
