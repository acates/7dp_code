# renamePxDicomFolders.py
# This script reads in a csv file of participant ids which are corss referenced
# against scan ids and renams the dicom folders appropriately

# library of functions for checking, making, renaming files etc.
import os as os
# library for handelling data read in from a csv
import pandas as pd

# where all our dicom folders are
# dataRoot = '/Users/cassandragouldvanpraag/Documents/WorkICloud/Teaching/h2mri/code/temp/testDicomFolder'
dataRoot = '/vols/Scratch/brc_em/7DP/sourcedata/dicom_ASL'

# csv file containing our index of scan IDs
Rand_code_data = pd.read_csv('7DP_IDs.csv')
# printing so we can see a list of the column headers
print(Rand_code_data)

# looping over each row in the data using the index 'p' and the iterrows function (indenting marks where the for loop works)
for index, p in Rand_code_data.iterrows():
    # creating the old path from the root and Scan_num from row p
    pxFolder_old = os.path.join(dataRoot,p.Scan_num)
    # creating the new path from the root and Rand_code from row p (%03d = telling python that the string should be padded with leading zeros)
    pxFolder_new = os.path.join(dataRoot,str('%03d' % p.Rand_code))

    # Creating some on screen log of what we are doing
    print('Checking for participant directory:', p.Scan_num)

    # if the new folder already exists (i.e. if there is already a folder renamed)
    if os.path.isdir(pxFolder_new):
        print( '-- Found directory:new folder already exists',pxFolder_new)
    # make the "new" copy separate from the "new""new"
        pxFolder_new_check = pxFolder_new + "_check"
        os.rename(pxFolder_new,pxFolder_new_check)
        print(' -- Renaming to: ', pxFolder_new_check)


    # if the old directory is found (os.path.isdir returns "true")
    if os.path.isdir(pxFolder_old):
        print(' -- Found directory:',pxFolder_old)
        # added some extra spaces below so the outputs were aligned
        print(' -- Renaming to:    ', pxFolder_new)
        # rename from old to new
        os.rename(pxFolder_old, pxFolder_new)
    # if it is not found (os.path,isdir returns "false")
    else:
        print(' -- Participant directory not found.')
